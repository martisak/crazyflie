pwm: pwm_test pwm_plots
yaw: yaw_test yaw_plots

pwm_test:
	python pwmlog.py

pwm_plots:
	Rscript plot_log.R

yaw_test:
	python yawlog.py

yaw_plots:
	Rscript plot_yaw.R