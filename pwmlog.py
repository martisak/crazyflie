import logging
import time
from threading import Thread
from util import get_logger

import ujson as json
import cflib.crtp  # noqa
from cflib.crazyflie import Crazyflie
from cflib.crazyflie.log import LogConfig

# Only output errors from the logging framework
logging.basicConfig(level=logging.ERROR)


class PWMLogging:
    """
    Simple logging example class that logs the Stabilizer from a supplied
    link uri and disconnects after 5s.
    """

    def __init__(self, link_uri):
        """ Initialize and run the example with the specified link_uri """

        self.logfile = "pwmlog.json"
        self.logfile_f = open(self.logfile, "w")
        self.logger = get_logger("PWMLogging")
        self._cf = Crazyflie(rw_cache='./cache')

        # Connect some callbacks from the Crazyflie API
        self._cf.connected.add_callback(self._connected)
        self._cf.disconnected.add_callback(self._disconnected)
        self._cf.connection_failed.add_callback(self._connection_failed)
        self._cf.connection_lost.add_callback(self._connection_lost)

        self.logger.debug('Connecting to %s' % link_uri)

        # Try to connect to the Crazyflie
        self._cf.open_link(link_uri)

        # Variable used to keep main loop occupied until disconnect
        self.is_connected = True

    def _connected(self, link_uri):
        """ This callback is called form the Crazyflie API when a Crazyflie
        has been connected and the TOCs have been downloaded."""
        self.logger.info('Connected to %s' % link_uri)

        # The definition of the logconfig can be made before connecting
        self._lg_stab = LogConfig(name='pwmLog', period_in_ms=10)
        # self._lg_stab.add_variable('stabilizer.roll', 'float')
        # self._lg_stab.add_variable('stabilizer.pitch', 'float')
        # self._lg_stab.add_variable('stabilizer.yaw', 'float')
        self._lg_stab.add_variable('pwmLog.roll', 'float')
        self._lg_stab.add_variable('pwmLog.pitch', 'float')
        self._lg_stab.add_variable('pwmLog.mPWM', 'float')
        self._lg_stab.add_variable('pwmLog.mID', 'float')

        # Adding the configuration cannot be done until a Crazyflie is
        # connected, since we need to check that the variables we
        # would like to log are in the TOC.
        try:
            self._cf.log.add_config(self._lg_stab)
            # This callback will receive the data
            self._lg_stab.data_received_cb.add_callback(self._stab_log_data)
            # This callback will be called on errors
            self._lg_stab.error_cb.add_callback(self._stab_log_error)
            # Start the logging
            self._lg_stab.start()
        except KeyError as e:
            self.logger.error('Could not start log configuration,'
                  '{} not found in TOC'.format(str(e)))
        except AttributeError:
            self.logger.error('Could not add Stabilizer log config, bad configuration.')

        # Add callback functions to our paramters of interest.
        self._cf.param.add_update_callback(group='pwmPar',
                                           name='mID',
                                           cb=self._a_pwm_callback)

        self._cf.param.add_update_callback(group='pwmPar',
                                           name='mPWM',
                                           cb=self._a_pwm_callback)

        Thread(target=self._ramp_motors).start()

    def _ramp_motors(self):

        # Set motor ID
        mIDs = [1, 2]

        for mID in mIDs:
            # Stepping through PWM values with step size 5000.
            # End with 0 to turn off motor.
            for mPWM in list(range(0, 2**16, 5000)) + [0]:

                self.logger.info("Setting motor {} to {}".format(mID, mPWM))

                self._cf.param.set_value('pwmPar.mID',
                                         '{:d}'.format(mID))

                self._cf.param.set_value('pwmPar.mPWM',
                                         '{:d}'.format(mPWM))

                # Make sure to stabilize (10 seconds could be enough)
                # Alt. check other variables
                time.sleep(10)

        self._cf.close_link()

    def _a_pwm_callback(self, name, value):
        """Callback for pid_attitude.pitch_kd"""
        self.logger.debug('Readback: {0}={1}'.format(name, value))

        # End the example by closing the link (will cause the app to quit)
        # self._cf.close_link()

    def _stab_log_error(self, logconf, msg):
        """Callback from the log API when an error occurs"""
        self.logger.error('Error when logging %s: %s' % (logconf.name, msg))

    def _stab_log_data(self, timestamp, data, logconf):
        """Callback froma the log API when data arrives"""

        data['timestamp'] = timestamp
        data['logconf'] = logconf.name
        self.logfile_f.write(json.dumps(data) + "\n")

        # print('[%d][%s]: %s' % (timestamp, logconf.name, data))

    def _connection_failed(self, link_uri, msg):
        """Callback when connection initial connection fails (i.e no Crazyflie
        at the speficied address)"""
        self.logger.error('Connection to %s failed: %s' % (link_uri, msg))
        self.is_connected = False

    def _connection_lost(self, link_uri, msg):
        """Callback when disconnected after a connection has been made (i.e
        Crazyflie moves out of range)"""
        self.logger.error('Connection to %s lost: %s' % (link_uri, msg))

    def _disconnected(self, link_uri):
        """Callback when the Crazyflie is disconnected (called in all cases)"""

        self.logger.info('Disconnected from %s' % link_uri)
        self.is_connected = False
        self.logfile_f.close()


if __name__ == '__main__':
    # Initialize the low-level drivers (don't list the debug drivers)
    cflib.crtp.init_drivers(enable_debug_driver=False)
    logger = get_logger("main")

    # Scan for Crazyflies and use the first one found
    logger.info('Scanning interfaces for Crazyflies...')
    available = cflib.crtp.scan_interfaces()
    logger.info('Crazyflies found:')

    for i in available:
        logger.info(i[0])

    if len(available) > 0:
        le = PWMLogging(available[0][0])
    else:
        logger.warning('No Crazyflies found, cannot run example')

    # The Crazyflie lib doesn't contain anything to keep the application alive,
    # so this is where your application should do something. In our case we
    # are just waiting until we are disconnected.
    while le.is_connected:
        time.sleep(5)
