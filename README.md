# Crazyflie Project

![Crazyflie 2.0](./images/crazyflie-2.png) 


## Instructions

### Preparations 

Mount the rod at the front of the Crazyflie, over `M4` and `M1`. Hang the Crazyflie so that it can rotate around the rod. In the picture above, cups with a sharp corner in the ear was used so that the Crazyflie wouldn't climb the ear during the test.

You should preferably run the following in a `virtualenv` with Python 3. First we install all Python libraries and other things.


```
brew install python3 sdl sdl2 sdl_image sdl_mixer sdl_ttf libusb portmidi pyqt5
```

```
cd crazyflie-lib-python
python setup.py install
cd ..
pip3 install -r requirements.txt
```

Now we will update the Crazyflie firmware.

```
git clone git@gitlab.com:martisak/crazyflie-firmware.git
cd crazyflie-firmware
git checkout pwm
tb build
```
 
Turn on the Crazyflie (in bootloader mode), plugin the radio unit and run flash the firmware via `cfclient`.

### Running the PWM test

It is important that all of the following is run in a completely dark room.

![Crazyflie 2.0](./images/crazyflie.jpg) 

Then you are ready to start the test. Not that this will run one motor, so make sure the rotor blades are cleared.

```
python pwmlog.py
```

This will write a log to `pwmlog.json` with lines looking like below. If you have `R`, you can plot the logs with

```
Rscript plot_log.R
```

Alternatively, you can run the test and make the plots with `make`.

### Example output

~~~{json}
{
  "pwmLog.roll": -0.6167603135,
  "pwmLog.pitch": -1.9944497347,
  "pwmLog.mPWM": 5000,
  "pwmLog.mID": 1,
  "timestamp": 1297066,
  "logconf": "pwmLog"
}
~~~

|  PWM values vs time  |   Roll versus time   | PWM value versus average roll |
|----------------------|----------------------|-------------------------------|
| ![](./images/pwmlog_mpwm.png) | ![](./images/pwmlog_roll.png) | ![](./images/pwmlog_pwm_vs_pitch.png)   |

## Lift calculations

From the above test we can predict the maximum takeoff weight and at which PWM settings the Crazyflie will take off.

|      Forces at rest     |  Forces at test |          Lift vs PWM           |
|-------------------------|-----------------|--------------------------------|
| ![](./images/forces_at_rest.png) | ![](./images/forces.png) | ![](./images/pwmlog_predicted_lift.png) |

We set the moments of the center of mass and the thrust to be equal and solve the equation for the thrust force. See `plot_log.R` for details on the calculations. predicted and actual values differ a bit, possibly due to aerodynamic effects?

## Questions

* **Question 1**: Is the relationship between motor signal (PWM) and generated force approximately linear, or are there better approximations?

    The file `plot_log.R` calculates a better prediction.

* **Question 2**: Knowing that we have four propellers and that the UAV weighs 27 grams, how much additional load do you estimate that we can possible fly with?

    We can fly (take off) with approximately 25 g load.

* **Question 3**: What do you observe? In which direction is the UAV rotating? What happens if you turn off M2 and use M1 instead? Or M3?

    In the opposite direction of the propellers.

* **Question 4**: Compare what happens when you use the diagonally opposite motors, M1 and M3, simultaneously compared to when only M1 is used? Does the total yaw torque increase or decrease?

    The total yaw torque increases, the center of rotation is in center of the crazyflie.

* **Question 5**: What happens when you use two neighboring motors, such as  M1 and M2, simultaneously? Does the total yaw torque increase or decrease?

    The total yaw torque cancels out.


## Milestone 1

Now we will update the Crazyflie firmware with some new PD-controller code we use in Milestone 1.

```
git clone git@gitlab.com:martisak/crazyflie-firmware.git
cd crazyflie-firmware
git checkout milestone1
tb build
```

Turn on the Crazyflie (in bootloader mode), plugin the radio unit and run flash the firmware via `cfclient`.

Using a PD-controller with P=0.05 and D=0.005 (set in `yawlog.py`) we get the figures below. First we see a figure illustrating the reference signal and current yaw, then we see a plot of the control signal and lastly we see the input for each motor. A design goal has been to make the input to each motor in the range of (0, 1), so therefore the control signal was adjusted using the controller gains (K_p and K_d).

You can also directly control these parameters in the `yawCtrlPar` group in `cfclient`.


|  Yaw and reference   |   Control signal   |         Motor input          |
|----------------------|--------------------|------------------------------|
| ![](./images/yawctrl_yaw.png) | ![](./images/yawctrl_u.png) | ![](./images/yawctrl_motors.png) |

<!-- blank line -->
<figure class="video_container">
  <iframe src="https://www.youtube.com/embed/bhK0KMUAWN0" frameborder="0" allowfullscreen="true"> </iframe>
</figure>
<!-- blank line -->



